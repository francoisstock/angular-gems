import { Component } from '@angular/core';

import { IRuby } from 'projects/rubies/src/lib';

@Component({
  selector: 'gpr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public readonly title = 'gems-private';
  public readonly gem: IRuby = {
    name: 'Wonder of dark',
    place: 'Ivory Coast'
  };
}
