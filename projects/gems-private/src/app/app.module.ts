import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RubiesModule } from 'projects/rubies/src/lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    RubiesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
