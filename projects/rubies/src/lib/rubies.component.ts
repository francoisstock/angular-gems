import { Component, OnInit, Input } from '@angular/core';
import { IRuby } from './ruby.model';

@Component({
  selector: 'lib-rubies',
  template: `
    <p>
      rubies works! See: {{ ruby.name }}
    </p>
  `,
  styles: []
})
export class RubiesComponent implements OnInit {
  @Input() public readonly ruby: IRuby;

  constructor() { }

  ngOnInit() {
  }

}
