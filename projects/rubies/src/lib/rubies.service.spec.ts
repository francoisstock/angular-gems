import { TestBed } from '@angular/core/testing';

import { RubiesService } from './rubies.service';

describe('RubiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RubiesService = TestBed.get(RubiesService);
    expect(service).toBeTruthy();
  });
});
