import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RubiesComponent } from './rubies.component';

describe('RubiesComponent', () => {
  let component: RubiesComponent;
  let fixture: ComponentFixture<RubiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RubiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RubiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
