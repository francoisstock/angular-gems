import { NgModule } from '@angular/core';
import { RubiesComponent } from './rubies.component';



@NgModule({
  declarations: [RubiesComponent],
  imports: [
  ],
  exports: [RubiesComponent]
})
export class RubiesModule { }
