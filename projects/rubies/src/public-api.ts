/*
 * Public API Surface of rubies
 */

export * from './lib/rubies.service';
export * from './lib/rubies.component';
export * from './lib/rubies.module';
