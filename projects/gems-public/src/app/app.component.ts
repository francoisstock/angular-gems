import { Component } from '@angular/core';

@Component({
  selector: 'gpu-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'gems-public';
}
